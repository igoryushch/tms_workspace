package ua.novaposhta.tms.bus.wialon;

import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;

import javax.ws.rs.*;

/**
 * Wialon APi adapter.
 *
 * @author Yuriy Tumakha
 */
@Path("/")
@Produces("application/json; charset=UTF-8")
public interface WialonService {

    @GET
    @Path("/login")
    @Description(value = "Creates new Wialon API session", target = DocTarget.METHOD)
    String login();

    @GET
    @Path("/searchItem")
    @Description(value = "Search item by ID", target = DocTarget.METHOD)
    String searchItem(@QueryParam("itemId") Long itemId,
                      @QueryParam(value = "flags")  @DefaultValue("5121") Integer flags);

    @GET
    @Path("/logout")
    @Description(value = "Closes Wialon API session", target = DocTarget.METHOD)
    String logout();

}
