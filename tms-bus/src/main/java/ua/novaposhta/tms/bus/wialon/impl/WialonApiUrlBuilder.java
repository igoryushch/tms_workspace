package ua.novaposhta.tms.bus.wialon.impl;

import org.springframework.beans.factory.annotation.Value;
import ua.novaposhta.tms.bus.wialon.WialonService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author Yuriy Tumakha
 */
public class WialonApiUrlBuilder implements WialonService {

    private String username;

    private String password;

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String login() {
        String params = String.format("{\"user\":\"%s\",\"password\":\"%s\"}", username, password);
        return getCommandUrlPath("core/login", params);
    }

    @Override
    public String logout() {
        return "svc=core/logout&params={}";
    }

    @Override
    public String searchItem(Long itemId, Integer flags) {
        String params = String.format("{\"itemId\":%d,\"flags\":%d}", itemId, flags);
        return getCommandUrlPath("core/search_item", params);
    }

    private String getCommandUrlPath(String command, String params) {
        params = urlEncode(params);
        return String.format("svc=%s&params=%s", command, params);
    }

    private String urlEncode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
