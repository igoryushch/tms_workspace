package ua.novaposhta.tms.bus.rest;

import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import ua.novaposhta.tms.bus.rest.impl.UserInfo;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.domain.User;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Dispatcher REST services.
 *
 * @author Yuriy Tumakha
 */
@Path("/")
@Produces("application/json; charset=UTF-8")
public interface DispatcherService {


    @GET
    @Path("/userinfo")
    @Description(value = "Returns current user info", target = DocTarget.METHOD)
    public UserInfo getUserInfo();

    @GET
    @Path("/availableCars")
    @Description(value = "Returns filtered available cars", target = DocTarget.METHOD)
    public List<Car> getAvailableCars(@QueryParam("search") String search);

    @GET
    @Path("/selectedCars")
    @Description(value = "Returns selected cars for current user", target = DocTarget.METHOD)
    public List<Car> getSelectedCars();

    @GET
    @Path("/selectCar")
    @Description(value = "Select car", target = DocTarget.METHOD)
    public String selectCar(@QueryParam("number") String number);

    @GET
    @Path("/unselectCar")
    @Description(value = "Unselect car", target = DocTarget.METHOD)
    public String unselectCar(@QueryParam("number") String number);

}
