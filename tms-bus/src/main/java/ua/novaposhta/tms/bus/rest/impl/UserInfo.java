package ua.novaposhta.tms.bus.rest.impl;

/**
 * @author Yuriy Tumakha
 */
public class UserInfo {

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

}
