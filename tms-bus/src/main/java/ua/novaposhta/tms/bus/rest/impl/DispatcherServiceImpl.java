package ua.novaposhta.tms.bus.rest.impl;

import ua.novaposhta.tms.bus.rest.DispatcherService;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.domain.User;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yuriy Tumakha
 */
public class DispatcherServiceImpl implements DispatcherService {

    private static final String OK_RESPONSE = "OK";

    @Override
    public UserInfo getUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setLogin("ext.tumakha.y");
        return userInfo;
    }

    @Override
    public List<Car> getAvailableCars(String search) {
        return getAllCars();
    }

    @Override
    public List<Car> getSelectedCars() {
        return getAllCars();
    }

    @Override
    public String selectCar(String number) {

        return OK_RESPONSE;
    }

    @Override
    public String unselectCar(String number) {

        return OK_RESPONSE;
    }


    private List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>(3);
        cars.add(newCar("АН7975СІ","Водій 1","Київ", 2827L));
        cars.add(newCar("АН7849НР","Водій 2","Київ", 2692L));
        cars.add(newCar("АН7318НХ","Водій 3","Київ", 2814L));
        return cars;
    }

    private Car newCar(String number, String driver, String city, Long wialonId) {
        Car car = new Car();
        car.setNumber(number);
        car.setDriver(driver);
        car.setCity(city);
        car.setWialonId(wialonId);
        return car;
    }
}
