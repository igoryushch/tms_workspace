package ua.novaposhta.tms.bus.wialon.impl;

import org.apache.camel.Exchange;
import org.apache.camel.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * @author Yuriy Tumakha
 */
public class WialonProcessor {

    private static final String LOGIN_ERROR_HEADER = "isLoginError";
    private static final String URI_PATH = "ajax.html";
    private static final Pattern SSID_PATTERN = Pattern.compile("ssid\":\"(.*?)\"");

    private String ssid;

    private WialonApiUrlBuilder urlBuilder;

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public void setUrlBuilder(WialonApiUrlBuilder urlBuilder) {
        this.urlBuilder = urlBuilder;
    }

    public void login(Exchange exchange) {
        setWialonQueryHeaders(exchange, urlBuilder.login());
    }

    public void logout(Exchange exchange) {
        setWialonQueryHeaders(exchange, urlBuilder.logout() + "&ssid=" + ssid);
    }

    public void searchItem(Exchange exchange) {
        Long itemId = null;
        Integer flags = 5121;
        try {
            Object itemIdObj = exchange.getIn().getHeader("itemId");
            Object flagsObj = exchange.getIn().getHeader("flags");
            if (itemIdObj != null) {
                itemId = itemIdObj instanceof Long ? new Long((Long) itemIdObj) : new Long(itemIdObj.toString());
            }
            if (flagsObj != null) {
                flags = flagsObj instanceof Long ? new Integer(((Long) flagsObj).intValue()) : new Integer(flagsObj.toString());
            }
        } catch (NumberFormatException e) {
            new IllegalArgumentException(e);
        }
        String query = urlBuilder.searchItem(itemId, flags) + "&ssid=" + ssid;
        setWialonQueryHeaders(exchange, query);
    }

    public void postlogin(Exchange exchange) throws Exception {
        String body = exchange.getIn().getBody(String.class);
        Matcher m = SSID_PATTERN.matcher(body);
        if (m.find()) {
            ssid = m.group(1);
        }
    }

    public void postlogout(Exchange exchange) {
        setSsid(null);
    }

    public void postsearchItem(Exchange exchange) {
        checkLoginError(exchange);
    }

    private void checkLoginError(Exchange exchange) {
        String body = exchange.getIn().getBody(String.class);
        setLoginErrorHeader(exchange, body.contains("error\":1"));
    }

    private void setWialonQueryHeaders(Exchange exchange, String queryString) {
        Message inMessage = exchange.getIn();
        inMessage.setHeader(Exchange.HTTP_URI, "/" + URI_PATH);
        inMessage.setHeader(Exchange.HTTP_PATH, URI_PATH);
        inMessage.setHeader(Exchange.HTTP_QUERY, queryString);
    }

    private void setLoginErrorHeader(Exchange exchange, boolean isLoginError) {
        exchange.getIn().setHeader(LOGIN_ERROR_HEADER, isLoginError);
    }

}
