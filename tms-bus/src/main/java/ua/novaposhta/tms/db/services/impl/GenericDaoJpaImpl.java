package ua.novaposhta.tms.db.services.impl;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * @author Yuriy Tumakha
 */
public abstract class GenericDaoJpaImpl<T> {

    @PersistenceContext(unitName="tmsjpa")
    protected EntityManager entityManager;

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<T> getAll() {
        Class<T> clazz = getGenericType();
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        query.select(query.from(clazz));
        return entityManager.createQuery(query).getResultList();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public T findById(Integer id) {
        Class<T> clazz = getGenericType();
        return entityManager.find(clazz, id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void create(T entity) {
        entityManager.persist(entity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public T update(T entity) {
        return entityManager.merge(entity);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @SuppressWarnings("unchecked")
    private Class<T> getGenericType() {
        return (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

}
