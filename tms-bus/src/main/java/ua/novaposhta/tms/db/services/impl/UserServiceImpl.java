package ua.novaposhta.tms.db.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.domain.User;
import ua.novaposhta.tms.db.services.CarService;
import ua.novaposhta.tms.db.services.UserService;

import java.util.Set;

/**
 * @author Yuriy Tumakha
 */
@Service("userService")
@Repository
public class UserServiceImpl extends GenericDaoJpaImpl<User> implements UserService {

    @Autowired
    private CarService carService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void selectCar(String number) {
        Car car = carService.findByNumber(number);
        User user = getCurrentUser();
        user.getCars().add(car);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void unselectCar(String number) {
        Car car = carService.findByNumber(number);
        User user = getCurrentUser();
        user.getCars().remove(car);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Set<Car> getUserCars() {
        User user = getCurrentUser();
        Set<Car> cars = user.getCars();
        cars.size();
        return cars;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User findByLogin(String username) {
        return entityManager.createQuery("SELECT u FROM User u WHERE u.login = ?1", User.class)
                .setParameter(1, username).getSingleResult();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public User getCurrentUser() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication == null || "anonymousUser".equals(authentication.getName())) {
//            throw new IllegalStateException("User not logged in");
//        }
//        String username = authentication.getName();
//        return findByLogin(username);
        return findByLogin("ext.tumakha.y");
    }

}
