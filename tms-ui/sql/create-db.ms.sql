CREATE TABLE TMS.CAR (
    ID [int] NOT NULL identity(1,1),
    NUMBER varchar(16) NOT NULL,
    DRIVER varchar(255) NULL,
    CITY varchar(255) NULL,
    WIALON_ID bigint NULL,
  CONSTRAINT PK_CAR PRIMARY KEY ( ID ASC ),
  CONSTRAINT UK_NUMBER UNIQUE (NUMBER)
)
GO

CREATE TABLE TMS.[USER] (
    ID [int] NOT NULL identity(1,1),
    LOGIN varchar(255) NULL,
  CONSTRAINT PK_USER PRIMARY KEY ( ID ASC ),
  CONSTRAINT UK_LOGIN UNIQUE (LOGIN)
)
GO

CREATE TABLE TMS.USER_CAR (
    USER_ID [int] NOT NULL,
    CAR_NUMBER varchar(16) NOT NULL,
 CONSTRAINT PK_USER_CAR PRIMARY KEY ( USER_ID ASC, CAR_NUMBER ASC ),
 CONSTRAINT FK_USER_CAR_NUMBER FOREIGN KEY (CAR_NUMBER) REFERENCES TMS.CAR (NUMBER),
 CONSTRAINT FK_CAR_USER_ID FOREIGN KEY (USER_ID) REFERENCES TMS.[USER] (ID)
)
GO