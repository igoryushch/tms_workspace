package ua.novaposhta.tms.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.services.CarService;
import ua.novaposhta.tms.db.services.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yuriy Tumakha
 */
@RestController
@RequestMapping("/webservice")
public class RestServiceController {

    private static final Log LOG = LogFactory.getLog(RestServiceController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CarService carService;

    @RequestMapping(value = "availableCars", method = RequestMethod.GET)
    public List<Car> getAvailableCars(@RequestParam("search") String search) {
        LOG.debug("search " + search);

        List<Car> cars = carService.filterByNumber(search);
        return cars;
    }

    @RequestMapping(value = "selectCar", method = RequestMethod.GET)
    public void selectCar(@RequestParam("number") String number) {
        LOG.debug("select car " + number);

        userService.selectCar(number);
    }

    @RequestMapping(value = "unselectCar", method = RequestMethod.GET)
    public void unselectCar(@RequestParam("number") String number) {
        LOG.debug("unselect car " + number);

        userService.unselectCar(number);
    }

}
