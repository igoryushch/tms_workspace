package ua.novaposhta.tms.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Yuriy Tumakha
 */
@ControllerAdvice
@RequestMapping("/")
public class RootController {

    @RequestMapping(method = RequestMethod.GET)
    public String get() {
        return "redirect:dispatcher-panel";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

}