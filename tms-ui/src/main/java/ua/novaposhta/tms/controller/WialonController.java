package ua.novaposhta.tms.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.novaposhta.tms.wialon.services.WialonService;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Yuriy Tumakha
 */
@RestController
@RequestMapping("/wialon")
public class WialonController {

    @Autowired
    private WialonService wialonService;

    @RequestMapping(value = "login")
    public String login() {
        return wialonService.login();
    }

    @RequestMapping(value = "logout")
    public String logout() {
        return wialonService.logout();
    }

    @RequestMapping(value = "searchItem")
    public String searchItem(@RequestParam("itemId") Long itemId,
                             @RequestParam(value = "flags", defaultValue = "5121") Integer flags,
                             HttpServletResponse response) {
        //response.setHeader("Content-Type", "text/plain; charset=utf-8");
        response.setHeader("Content-Type", "application/json; charset=utf-8");
        return wialonService.searchItem(itemId, flags);
    }

}