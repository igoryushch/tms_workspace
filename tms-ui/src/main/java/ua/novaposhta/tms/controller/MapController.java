package ua.novaposhta.tms.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URL;

/**
 * @author Yuriy Tumakha
 */
@ControllerAdvice
@RequestMapping("/map")
public class MapController {

    private static final String MAP_BASE_URL = "http://tiles.mq.ua";
    private static final Log LOG = LogFactory.getLog(MapController.class);

    @RequestMapping(value = "/**", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getTileImage(HttpServletRequest request) throws IOException {

        //TODO: use cache/proxy

        //String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String path = request.getServletPath().replace("/map", "");
        URL url = new URL(MAP_BASE_URL + path);
        //LOG.debug(url);
        byte[] bytes = StreamUtils.copyToByteArray(new BufferedInputStream(url.openStream()));

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.CREATED);
    }

}
