package ua.novaposhta.tms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.domain.User;
import ua.novaposhta.tms.db.services.UserService;
import ua.novaposhta.tms.wialon.services.WialonService;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @author Yuriy Tumakha
 */
@ControllerAdvice
@RequestMapping("/dispatcher-panel")
public class DispatcherPanelController {

    @Autowired
    private UserService userService;

    @Autowired
    private WialonService wialonService;

    @RequestMapping(method = RequestMethod.GET)
    public String show(HttpServletRequest request) {
        Set<Car> userCars = userService.getUserCars();
        request.setAttribute("userCars", userCars);
        request.setAttribute("wialonHost", wialonService.getWialonHost());
        return "dispatcher";
    }

}