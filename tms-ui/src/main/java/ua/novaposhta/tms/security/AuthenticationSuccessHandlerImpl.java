package ua.novaposhta.tms.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import ua.novaposhta.tms.db.domain.User;
import ua.novaposhta.tms.db.services.UserService;

import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Yuriy Tumakha
 */
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    @Autowired
    private UserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // Create user in DB if not exists
        try {
            userService.getCurrentUser();
        } catch (NoResultException ex) {
            User user = new User();
            user.setLogin(SecurityContextHolder.getContext().getAuthentication().getName());
            userService.create(user);
        }
        response.sendRedirect(request.getContextPath());
    }

}
