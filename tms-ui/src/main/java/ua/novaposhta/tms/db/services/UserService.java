package ua.novaposhta.tms.db.services;

import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.domain.User;

import java.util.List;
import java.util.Set;

/**
 * @author Yuriy Tumakha
 */
public interface UserService {

    void create(User user);

    User update(User user);

    void delete(User user);

    User findById(Integer userId);

    User findByLogin(String username);

    List<User> getAll();

    void selectCar(String number);

    void unselectCar(String number);

    Set<Car> getUserCars();

    User getCurrentUser();

}