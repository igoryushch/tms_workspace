package ua.novaposhta.tms.db.services.impl;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ua.novaposhta.tms.db.domain.Car;
import ua.novaposhta.tms.db.services.CarService;

import java.util.List;

/**
 * @author Yuriy Tumakha
 */
@Service("carService")
@Repository
public class CarServiceImpl extends GenericDaoJpaImpl<Car> implements CarService {

    private static final int MAX_RESULTS = 1000;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<Car> filterByNumber(String pattern) {
        return entityManager.createQuery("SELECT c FROM Car c WHERE c.number LIKE ?1 ORDER BY c.number", Car.class)
                .setParameter(1, "%" + pattern.toUpperCase() + "%").setMaxResults(MAX_RESULTS).getResultList();
    }

    @Override
    public Car findByNumber(String number) {
        return entityManager.createQuery("SELECT c FROM Car c WHERE c.number = ?1", Car.class)
                .setParameter(1, number).getSingleResult();
    }

}