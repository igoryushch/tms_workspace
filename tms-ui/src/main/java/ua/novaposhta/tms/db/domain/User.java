package ua.novaposhta.tms.db.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Yuriy Tumakha
 */
@Entity
@Table(name = "[USER]")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String login;

    @ManyToMany
    @JoinTable(name = "USER_CAR", joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "CAR_NUMBER", referencedColumnName = "number"))
    private Set<Car> cars;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

}
