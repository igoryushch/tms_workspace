package ua.novaposhta.tms.db.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yuriy Tumakha
 */
@Entity
@Table(name = "CAR")
public class Car implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true, length = 16)
    private String number;

    private String driver;

    private String city;

    @Column(name="WIALON_ID")
    private Long wialonId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getWialonId() {
        return wialonId;
    }

    public void setWialonId(Long wialonId) {
        this.wialonId = wialonId;
    }

}
