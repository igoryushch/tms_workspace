package ua.novaposhta.tms.db.services;

import ua.novaposhta.tms.db.domain.Car;

import java.util.List;

/**
 * @author Yuriy Tumakha
 */
public interface CarService {

    void create(Car car);

    Car update(Car car);

    void delete(Car car);

    Car findById(Integer carId);

    //List<Car> getAll();

    List<Car> filterByNumber(String pattern);

    Car findByNumber(String number);

}