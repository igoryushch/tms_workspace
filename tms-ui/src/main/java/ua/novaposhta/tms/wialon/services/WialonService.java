package ua.novaposhta.tms.wialon.services;

/**
 * @author Yuriy Tumakha
 */
public interface WialonService {

    String login();
    String logout();
    String searchItem(Long itemId, Integer flags);

    String getWialonHost();

}
