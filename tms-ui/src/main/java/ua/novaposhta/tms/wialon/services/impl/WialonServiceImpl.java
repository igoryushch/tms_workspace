package ua.novaposhta.tms.wialon.services.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ua.novaposhta.tms.wialon.services.WialonService;

import javax.annotation.PostConstruct;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Yuriy Tumakha
 */
@Service("wialonService")
@Repository
public class WialonServiceImpl implements WialonService {

    private static final Log LOG = LogFactory.getLog(WialonServiceImpl.class);
    private static final Pattern SSID_PATTERN = Pattern.compile("ssid\":\"(.*?)\"");

    @Value("${wialon.host}")
    private String host;

    @Value("${wialon.username}")
    private String username;

    @Value("${wialon.password}")
    private String password;

    @Autowired
    private RestTemplate restTemplate;

    private String ssid;

    @Override
    public String login() {
        String params =  "{\"user\":\"" + username + "\",\"password\":\"" + password + "\"}";
        String result = requestJsonStringWithoutSSID("svc=core/login&params={params}", params);
        Matcher m = SSID_PATTERN.matcher(result);
        if (m.find()) {
            ssid = m.group(1);
        }
        return result;
    }

    @Override
    public String logout() {
        return runCommand("svc=core/logout&params={params}", "{}");
    }

    @Override
    public String searchItem(Long itemId, Integer flags) {
        String params = "{\"itemId\":" + itemId + ",\"flags\":" + flags + "}";
        return runCommand("svc=core/search_item&params={params}", params);
    }

    @Override
    public String getWialonHost() {
        return host;
    }

    private String runCommand(String commandUrlPath, Object... urlVariables) {
        int attempt = 1;
        do {
            String jsonData = requestJsonString(commandUrlPath, urlVariables);
            if (jsonData.contains("error")) {
                LOG.error(commandUrlPath + " " + Arrays.asList(urlVariables) + " Response:" + jsonData);
            }
            if (jsonData.contains("error\":1") && !commandUrlPath.contains("core/logout") && attempt < 2) {
                login();
                continue;
            }
            return jsonData;
        } while (attempt++ < 2);
        return null;
    }

    private String requestJsonStringWithoutSSID(String commandUrlPath, Object... urlVariables) {
        return restTemplate.getForObject(createUrl(commandUrlPath, true), String.class, urlVariables);
    }

    private String requestJsonString(String commandUrlPath, Object... urlVariables) {
        // Add "ssid" as first array element
        Object[] variables = new Object[urlVariables.length + 1];
        System.arraycopy(urlVariables, 0, variables, 1, urlVariables.length );
        variables[0] = ssid;
        return restTemplate.getForObject(createUrl(commandUrlPath, false), String.class, variables);
    }

    private String createUrl(String commandUrlPath, boolean withoutSSID) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(host);
        sb.append("/ajax.html?");
        if (!withoutSSID) {
            sb.append("ssid={ssid}&");
        }
        sb.append(commandUrlPath);
        return sb.toString();
    }

}