var resultTable = null;
var availableCars = null;
var map = null;
var markers = new Object();
var refitBounds = false;

$(function () {
    $("#search-car").autocomplete({
        minLength: 2,
        delay: 300,
        source: function (request, response) {
            $.ajax({
                url: "webservice/availableCars",
                dataType: "json",
                data: {
                    search: $("#search-car").val()
                },
                success: function (data) {
                    availableCars = data;
                    response($.map(availableCars, function (item) {
                        return {
                            label: item.number,
                            value: item.number
                        }
                    }));
                }
            });
        },
        close: function (event, ui) {
            $("#addCar").prop('disabled', false);
        }
    });
    $("#search-car").keypress(function (e) {
        if (e.which == 13) {
            selectCar();
        }
    });
    $("#addCar").click(function () {
        selectCar();
    });
});

function selectCar() {
    var carNumber = $("#search-car").val();

    if ($("#selected-cars").text().indexOf(carNumber) != -1) {
        return;
    }
    var cars = availableCars;
    $.each(cars, function (index, carData) {
        if (carNumber == carData.number) {
            resultTable.row.add([
            carData.wialonId, carData.number, carData.driver, carData.city]).draw();
            $.get("webservice/selectCar", {
                number: carNumber
            });
            return false;
        }
    });
}

function deleteCar(el) {
    var delRow = getClosestRow(el);
    var carNumber = delRow.data()[1];
    delRow.remove().draw(false);
    $.get("webservice/unselectCar", {
        number: carNumber
    });
}

function getClosestRow(el) {
    var targetRow = el.closest("tr").get(0);
    return resultTable.row(targetRow);
}

$.extend($.fn.dataTable.defaults, {
    "searching": false,
    "paginate": false,
    //"paging": false,
});

$(document).ready(function () {
    resultTable = $('#selected-cars').DataTable({
        "oLanguage": {
            "sEmptyTable": "Автомобили не выбраны",
            //"sInfo": "Выбрано _TOTAL_ автомобилей",
            "sInfo": "",
            "sInfoEmpty": ""
        },
        "searching": false,
        "paginate": false,
        "pageLength": 50,
        "ordering": true,
        "order": [
            [1, "asc"]
        ],
        "columnDefs": [
            {
                "targets": 0,
                "visible": false,
                "searchable": false
            },
            {
                "targets": 1,
                "sWidth": "85px",
                "render": function ( data, type, row ) {
                    var html = '';
                    if (row[0]) {
                        html += '<input type="checkbox" class="wialonCheckbox" name="wialonId[]" value="' + row[0] + '">';
                    }
                    return html + data;
                }
            },
            {
                "targets": 3,
                "render": function ( data, type, row ) {
                    return data + ' <a class="removeRow leaflet-popup-close-button" href="#">×</a>';
                }
            }
        ]
    });

    $("#checkAllCars").click(function(e) {
        $('input.wialonCheckbox').prop('checked', this.checked);
        e.stopPropagation();
    });

    initMap();
    setTimeout(redrawWialonItems, 2000);// first delay
});

$( document ).on( "click", "#selected-cars .removeRow", function() {
    deleteCar($(this));
});

function initMap() {
    // local server url:  L.tileLayer('map/{z}/{x}/{y}.png', {
    var luxenaLayer = L.tileLayer('http://tiles.mq.ua/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="http://maps.luxena.com/">Luxena</a>'
                    });
    var googleLayer = new L.Google('ROADMAP');
    map = L.map('map', {
        center: [50.434487, 30.503388],
        zoom: 11,
        layers: [luxenaLayer]
    });
    var baseMaps = {
        "Luxena": luxenaLayer,
        "Google": googleLayer
    };
    L.control.layers(baseMaps).addTo(map);
}

function redrawWialonItems() {
    $("input.wialonCheckbox:not(:checked)").each(function() {
        var wialonId = $(this).val();
        if (markers[wialonId]) {
            refitBounds = true;
            map.removeLayer(markers[wialonId]);
            markers[wialonId] = null;
        }
    });
    $("input.wialonCheckbox:checked").each(function() {
        var wialonId = $(this).val();
        $.get("wialon/searchItem", { itemId: wialonId }, function( data ) {
            var carNumber = data.nm;
            var imageChangeCounter = data.ugi;
            var latitude = data.pos.y;
            var longitude = data.pos.x;
            var locationAddress = data.pos.l;
            if (!markers[wialonId]) {
                refitBounds = true;
                var iconUrl = 'http://' + wialonHost + '/avl_icon/get/' + wialonId + '/32/' + imageChangeCounter + '.png';
                var carIcon = L.divIcon({
                    className: 'car-div-icon',
                    iconSize: [32, 32],
                    html: '<img src="' + iconUrl + '" height="32" width="32"/><br/><div class="car-label">' + carNumber + '</div>'
                });
                markers[wialonId] = L.marker([latitude,longitude], {icon: carIcon, title: locationAddress});
                map.addLayer(markers[wialonId]);
            }
            markers[wialonId].setLatLng([latitude,longitude]).update();
        }, "json");
    });
    if (refitBounds) {
        refitBounds = false;
        var bounds = [];
        $.each(markers, function( key, value ) {
            if (value) {
                bounds.push(value.getLatLng());
            }
        });
        if (bounds.length > 1) {
            map.fitBounds(bounds);
        } else if (bounds.length == 1) {
            map.setView(bounds[0]);
        }
    }
    setTimeout(redrawWialonItems, 1000);
}