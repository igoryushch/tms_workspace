<%@ page session="false" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<spring:url value="/j_spring_security_check" var="loginUrl" />
<html>
<head>
    <title>Login</title>
</head>
<body style="text-align: center; padding-top: 15%;">

<form action="${loginUrl}" method="POST">
    <c:if test="${!empty param.login_error}">
        <p style="color: red;">
            Invalid username or password.
        </p>
    </c:if>
    <p>
        <label for="j_username">Username</label>
        <input type="text" id="j_username" name="j_username"/>
    </p>
    <p>
        <label for="j_password">Password</label>
        <input type="password" id="j_password" name="j_password"/>
    </p>
    <button type="submit" class="btn">Log in</button>
</form>

</body>
</html>