<%@ page session="false" contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Dispatcher Panel</title>
    <spring:url value="/css/" var="css_base" />
    <spring:url value="/js/" var="js_base" />
    <script src="${js_base}jquery-1.11.1.min.js"></script>
    <script src="${js_base}jquery-ui-1.10.4.custom.min.js"></script>
    <script src="${js_base}jquery.dataTables.min.js"></script>
    <script src="${js_base}leaflet-0.7.3/leaflet.js"></script>
    <script src="http://maps.google.com/maps/api/js?v=3.2&sensor=false"></script>
    <script src="${js_base}leaflet-0.7.3/layer/tile/Google.js"></script>
    <script type="text/javascript">
        var wialonHost = '${wialonHost}';
    </script>
    <script src="${js_base}script.js"></script>

   <link rel="stylesheet" href="${css_base}smoothness/jquery-ui-1.10.4.custom.min.css"/>
    <link rel="stylesheet" href="${css_base}jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="${css_base}leaflet-0.7.3/leaflet.css" />
    <link rel="stylesheet" href="${css_base}style.css"/>
</head>
<body>

<table cellspacing="15">
    <tr><td colspan="2" style="text-align: right;">
        <security:authorize access="isAuthenticated()">
            <security:authentication property="principal.username" />
        </security:authorize>
        <span> | </span>
        <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
    </td></tr>
    <tr><td valign="top">

        <div class="ui-widget">
            <label for="search-car"><spring:message code="search.car.number"/></label>
            <input id="search-car" style="width: 100px;">
            <input type="button" id="addCar" value="+" disabled="true"/>
        </div>

        <div id="selected-cars-label" class="ui-widget">
            <label for="selected-cars"><spring:message code="selected.cars"/></label>
        </div>

        <table id="selected-cars" class="display cell-border" cellspacing="0" width="400">
        <thead>
        <tr>
            <th>wialonId</th>
            <th><div id="checkAllBlock"><input type="checkbox" id="checkAllCars"/></div><spring:message code="car.number"/></th>
            <th><spring:message code="driver.name"/></th>
            <th><spring:message code="city"/></th>
        </tr>
        </thead>
        <c:if test="${!empty userCars}">
        <tbody>
        <c:forEach items="${userCars}" var="car">
          <tr>
            <td>${car.wialonId}</td>
            <td>${car.number}</td>
            <td>${car.driver}</td>
            <td>${car.city}</td>
          </tr>
        </c:forEach>
        </tbody>
        </c:if>
        </table>

    </td><td>

    <div id="map"></div>

    </td></tr>
</table>

</body>
</html>