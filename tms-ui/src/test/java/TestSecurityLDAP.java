import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Yuriy Tumakha
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext-security-test.xml"})
public class TestSecurityLDAP {

    private static final String TEST_USER = "jenkins";
    private static final String TEST_PASSWORD = "j7nk!ns";

    @Autowired
    AuthenticationManager authenticationManager;

    @Test
    public void testAuthentication() {
        Authentication auth = this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(TEST_USER, TEST_PASSWORD));
        Assert.assertNotNull("LDAP login failed", auth);
        System.out.println("AUTH: " + auth.getAuthorities());
    }

}
